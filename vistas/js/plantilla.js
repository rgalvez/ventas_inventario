

$(function () {
    $("#tabla").DataTable(
      {"language": {

    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
    "sFirst":    "Primero",
    "sLast":     "Último",
    "sNext":     "Siguiente",
    "sPrevious": "Anterior"
    },
    "oAria": {
      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
  },
     "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": [  {
            extend: 'copy',
            text: 'Copia',
        },
         'csv',
        'excel',
        'pdf',
        {
            extend: 'print',
            text: 'Imprimir',
            autoPrint: true
        },
        {
            extend: 'colvis',
            text: 'Ocultar Columnas',
            postfixButtons: [ 'colvisRestore' ]
        }]  

      }).buttons().container().appendTo('#tabla_wrapper .col-md-6:eq(0)');
    
  });

   