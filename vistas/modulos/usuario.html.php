<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Administrar Usuarios</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="inicio">Inicio</a></li>
              <li class="breadcrumb-item active">Administrar Usuarios</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          
          <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarUsuario">Agregar Usuario</button>

        </div>
        <div class="card-body">
          <table class="table table-bordered table-striped  table-hover" id="tabla">
          
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nombre</th>
                    <th>Usuario</th>
                    <th>Foto</th>
                     <th>Perfil</th>
                    <th>Estado</th>
                    <th>Ultimo Login</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>Usuario Administrador</td>
                    <td>Admin</td>
                    <td><img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail" width="40px"></td>
                    <td>Administrador</td>
                    <td><button class="btn btn-success btn-xs">Activado</button></td>
                    <td>2017-12-11 12:05:32</td>
                    <td>
                      <div class="btn-group">
                        <button class="btn btn-warning"><i class="fa fa-pen"></i></button>

                        <button class="btn btn-danger"><i class="fa fa-times"></i></button>
                        
                      </div>
                    </td>
                  </tr>
                   <tr>
                    <td>1</td>
                    <td>Usuario Administrador</td>
                    <td>Admin</td>
                    <td><img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail" width="40px"></td>
                    <td>Administrador</td>
                    <td><button class="btn btn-danger btn-xs">Activado</button></td>
                    <td>2017-12-11 12:05:32</td>
                    <td>
                      <div class="btn-group">
                        <button class="btn btn-warning"><i class="fa fa-pen"></i></button>

                        <button class="btn btn-danger"><i class="fa fa-times"></i></button>
                        
                      </div>
                    </td>
                  </tr>
                </tbody>         
          

          </table>

        </div>
        <!-- /.card-body -->
       
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

  </div>
    <!--MODAL-->

  <div class="modal fade" id="modalAgregarUsuario" name="modalAgregarUsuario">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form role="form" method="post" enctype="multipart/form-data">

            <div class="modal-header" style="background:#007bff; color:white">
              <h4 class="modal-title">Agregar Usuario</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <div class="box-body">

                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                    <input class="form-control input-lg" type="text" name="nuevoNombre" placeholder="Ingresar Nombre" required>
                  </div>
                </div>

                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-text"><i class="fa fa-key"></i></span>
                    <input class="form-control input-lg" type="text" name="nuevoUsuario" placeholder="Ingresar Usuario" required>
                  </div>
                </div>

                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-text"><i class="fa fa-lock"></i></span>
                    <input class="form-control input-lg" type="password" name="nuevoPassword" placeholder="Ingresar Contraseña" required>
                  </div>
                </div>

                <!--Entrada para seleccionar Perfil-->
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-text"><i class="fa fa-users"></i></span>
                    <select class="form-control input-lg " name="nuevoPerfil">
                      <option value="">Seleccionar Perfil</option>
                      <option value="Administrador">Administrador</option>
                      <option value="Especial">Especial</option>
                      <option value="Vendedor">Vendedor</option>
                      
                    </select>
                  </div>
                </div>
                <!--Entrada para subir foto-->

                <div class="form-group">
                  <div class="panel">SUBIR FOTO</div>
                  <input type="file" name="nuevaFoto" name="nuevaFoto">
                  <p class="help-block">Peso máximo de la foto 200MB</p>
                  <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail" width="60px">  
                </div>
                
              </div>
            </div>

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
              <button type="submit" class="btn btn-primary">Guardar Usuario</button>
            </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

