<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Administrar Usuarios</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="inicio">Inicio</a></li>
              <li class="breadcrumb-item active">Administrar Usuarios</li>
            </ol>
          </div>
        </div>
        
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          
          <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarUsuario">Agregar Usuario</button>

        </div>
        <div class="card-body">
          <table class="table table-bordered table-striped  table-hover" id="tabla">
          
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nombre</th>
                    <th>Usuario</th>
                    <th>Foto</th>
                     <th>Perfil</th>
                    <th>Estado</th>
                    <th>Ultimo Login</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $item=null;
                  $valor=null;
                  $usuarios=ControladorUsuarios::ctrMostrarUsuarios($item,$valor);
                  
                  foreach ($usuarios as $key => $value) {
                  echo '<tr>
                    <td>'.$value["idUsuario"].'</td>
                    <td>'.$value["nombre"].'</td>
                    <td>'.$value["usuario"].'</td>';
                    if($value["foto"]!=""){
                      echo '<td><img src="'.$value["foto"].'" class="img-thumbnail" width="40px"></td>';  
                    }else{
                       echo '<td><img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail" width="40px"></td>';
                    }
                  
                   echo' <td>'.$value["perfil"].'</td>';
                   if($value['estado']!=0){
                    echo '<td><button class="btn btn-success btn-xs btnActivar" idUsuario="'.$value["idUsuario"].'" estadoUsuario="0">Activado</button></td>';
                    }else{
                     echo '<td><button class="btn btn-danger btn-xs btnActivar" idUsuario="'.$value["idUsuario"].'" estadoUsuario="1">Desactivado</button></td>';

                    }

                    echo '<td>'.$value["ultimo_login"].'</td>
                    <td>
                      <div class="btn-group">
                        <button class="btn btn-warning btnEditarUsuario" idUsuario="'.$value["idUsuario"].'" data-toggle="modal" data-target="#modalEditarUsuario"><i class="fa fa-pen"></i></button>

                        <button class="btn btn-danger btnEliminarUsuario" idUsuario="'.$value["idUsuario"].'" fotoUsuario="'.$value["foto"].'" usuario="'.$value["usuario"].'"><i class="fa fa-times"></i></button>
                        
                      </div>
                    </td>
                  </tr>';
                  }
                  ?>

                  
                  
                </tbody>         
          

          </table>

        </div>
        <!-- /.card-body -->
       
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

  </div>
  <!--MODAL-->

  <div class="modal fade" id="modalAgregarUsuario" name="modalAgregarUsuario">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form role="form" method="post" enctype="multipart/form-data">

            <div class="modal-header" style="background:#007bff; color:white">
              <h4 class="modal-title">Agregar Usuario</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <div class="box-body">

                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                    <input class="form-control input-lg" type="text" name="nuevoNombre" placeholder="Ingresar Nombre" required>
                  </div>
                </div>

                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-text"><i class="fa fa-key"></i></span>
                    <input class="form-control input-lg" type="text" name="nuevoUsuario" id="nuevoUsuario" placeholder="Ingresar Usuario" required>
                  </div>
                </div>

                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-text"><i class="fa fa-lock"></i></span>
                    <input class="form-control input-lg" type="password" name="nuevoPassword" placeholder="Ingresar Contraseña" required>
                  </div>
                </div>

                <!--Entrada para seleccionar Perfil-->
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-text"><i class="fa fa-users"></i></span>
                    <select class="form-control input-lg " name="nuevoPerfil">
                      <option value="">Seleccionar Perfil</option>
                      <option value="Administrador">Administrador</option>
                      <option value="Especial">Especial</option>
                      <option value="Vendedor">Vendedor</option>
                      
                    </select>
                  </div>
                </div>
                <!--Entrada para subir foto-->

                <div class="form-group">
                  <div class="panel">SUBIR FOTO</div>
                  <input type="file" class="nuevaFoto" id="nuevaFoto" name="nuevaFoto">
                  <p class="help-block">Peso máximo de la foto 2MB</p>
                  <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail previzualizar" width="60px">  
                </div>
             
                
              </div>
            </div>

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
              <button type="submit" class="btn btn-primary">Guardar Usuario</button>
            </div>
            <?php
              $crearUsuario = new ControladorUsuarios();
              $crearUsuario -> ctrCrearUsuario();
            ?>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <!--modal editar usuario-->
    <!--MODAL-->

  <div class="modal fade" id="modalEditarUsuario" name="modalEditarUsuario">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form role="form" method="post" enctype="multipart/form-data">

            <div class="modal-header" style="background:#007bff; color:white">
              <h4 class="modal-title">Editar Usuario</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <div class="box-body">

                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                    <input class="form-control input-lg" type="text"  id="editarNombre" name="editarNombre" value=""  required>
                  </div>
                </div>

                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-text"><i class="fa fa-key"></i></span>
                    <input class="form-control input-lg" type="text" id="editarUsuario" name="editarUsuario" value="" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-text"><i class="fa fa-lock"></i></span>
                    <input class="form-control input-lg" type="password"  name="editarPassword" placeholder="Escriba la nueva Contraseña" >
                    <input type="hidden" class="form-control input-lg" name="passwordActual" id="passwordActual" placeholder="Escriba la nueva Contraseña" required>
                  </div>
                </div>

                <!--Entrada para seleccionar Perfil-->
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-text"><i class="fa fa-users"></i></span>
                    <select class="form-control input-lg " name="editarPerfil">
                      <option value="" id="editarPerfil"></option>
                      <option value="Administrador">Administrador</option>
                      <option value="Especial">Especial</option>
                      <option value="Vendedor">Vendedor</option>
                      
                    </select>
                  </div>
                </div>
                <!--Entrada para subir foto-->

                <div class="form-group">
                  <div class="panel">SUBIR FOTO</div>
                  <input type="file" class="nuevaFoto" id="editarFoto" name="editarFoto">
                  <p class="help-block">Peso máximo de la foto 2MB</p>
                  <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail previzualizar" width="60px"> 
                  <input type="hidden" id="fotoActual" name="fotoActual"> 
                </div>
             
                
              </div>
            </div>

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
              <button type="submit" class="btn btn-primary">Modificar Usuario</button>
            </div>
            <?php
              $editarUsuarios = new ControladorUsuarios();
              $editarUsuarios -> ctrEditarUsuario();
            ?>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <!--fin modal editar usuario-->

   <?php
        $borrarUsuario = new ControladorUsuarios();
        $borrarUsuario -> ctrBorrarUsuario();
  ?>

