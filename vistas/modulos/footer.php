 <footer class="main-footer">
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io" target="_blank">Ventas</a>.</strong>
    TODOS LOS DERECHOS RESERVADOS.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0
    </div>
  </footer>