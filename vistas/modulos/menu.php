 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="vistas/img/plantilla/logo.png" alt="Ricardo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Ricardo Galvez</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="vistas/img/plantilla/logo.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Ricardo Galvez</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="inicio" class="nav-link active">
              <i class="nav-icon fas fa-home"></i>
              <p><span class="">Inicio</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="usuario" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p><span class="">Usuarios</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="categoria" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p><span class="">Categorias</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="producto" class="nav-link">
              <i class="nav-icon fas fa-id-card"></i>
              <p><span class="">Productos</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="cliente" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p><span class="">Clientes</span>
              </p>
            </a>
          </li>
          <!--Arbol Menu Despegable-->
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-list-ul"></i>
              <p>
                Ventas
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="administrar_venta" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Administrar Ventas</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="crear_venta" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Crear Ventas</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="reporte_venta" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Reporte Ventas</p>
                </a>
              </li>
            </ul>
          </li>
         
          
          
        
          
      
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>