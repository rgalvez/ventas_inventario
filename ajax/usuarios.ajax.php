<?php 
require_once "../controladores/usuario.controlador.php";
require_once "../modelos/MUsuario.php";
Class AjaxUsuarios 
{
	//EDityar Usuarios
	public $idUsuario;
	public function  ajaxEditarUsuario(){
		$item="idUsuario";
		$valor=$this->idUsuario;
		$respuesta = ControladorUsuarios::ctrMostrarUsuarios($item,$valor);
		echo json_encode($respuesta);
	}
	
//ACtivar Usuario
	public $activarId;
	public $activarUsuario;
	
 public function ajaxActivarUsuario(){
		$tabla="usuarios";

		$item1="estado";
		$valor1=$this->activarUsuario;

		$item2="idUsuario";
		$valor2=$this->activarId;
		$respuesta= ModeloUsuarios::MdlActulizarUsuario($tabla,$item1, $valor1,$item2, $valor2);

	}


	//VALIDAR NO REPETIR USUARIO
	public $validarUsuario;
	 public function ajaxValidarUsuario(){
		$item="usuario";
		$valor=$this->validarUsuario;
		$respuesta = ControladorUsuarios::ctrMostrarUsuarios($item,$valor);
		echo json_encode($respuesta);


	}

	
}
//Editar Usuario
if(isset($_POST["idUsuario"])){
		$editar = new AjaxUsuarios();
		$editar -> idUsuario=$_POST["idUsuario"];
		$editar -> ajaxEditarUsuario();
}

//Activar Usuario

if(isset($_POST["activarUsuario"])){
	
		$activarUsuario = new AjaxUsuarios();
		$activarUsuario -> activarUsuario = $_POST["activarUsuario"];
		$activarUsuario -> activarId = $_POST["activarId"];
		$activarUsuario ->ajaxActivarUsuario();
}
//VALIDAR NO REPETIR USUARIO
if(isset($_POST["validarUsuario"])){
		$valUsuario = new AjaxUsuarios();
		$valUsuario -> validarUsuario=$_POST["validarUsuario"];
		$valUsuario -> ajaxValidarUsuario();
}

?>